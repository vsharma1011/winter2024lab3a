//import java.util.Scanner;	

public class Application {
	public static void main(String[] arg){
		//Intitalizing studentOne and defining the associated fields
		Student studentOne = new Student();
		studentOne.grade = 86.2;
		studentOne.hasEveningClasses = true;
		studentOne.currentYear = 1;
		studentOne.program = "Computer Science";
		//Intitalizing studentTwo and defining the associated fields
		Student studentTwo = new Student();
		studentTwo.grade = 58.7;
		studentTwo.hasEveningClasses = false;
		studentTwo.currentYear = 2;
		studentTwo.program = "Pure and Applied";
		
		Student[] section4 = new Student[3];
		section4[0] = studentOne;
		section4[1] = studentTwo;
		section4[2] = new Student();
		section4[2].grade = 71.3;
		System.out.println(section4[1].grade);
		//Calls instance methods in student class
		studentTwo.greeting();
		studentTwo.movingUp();
	}
}