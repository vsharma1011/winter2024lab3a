//import java.util.Scanner;	

public class Student {
	//Fields for the student
	public double grade;
	public int currentYear;
	public String program;
	public boolean hasEveningClasses;
	
	public void movingUp(){
		if(this.grade >= 60 ){
			currentYear++;
			System.out.println("I will move on to year " + currentYear);
		}
		else{
			System.out.println("I need to redo some classes...");
		}
		
	}
	public void greeting(){
		System.out.println("I am a student who studies " + program);
		if(hasEveningClasses){
			System.out.println("I am also taking evening classes");
		}
	}
}